%{
    open Syntax
%}

%token <string> VAR
%token <Syntax.vtype> CONST
%token <float> FCONST
%token <Syntax.vtype> STRING
%token NOT
%token FUNC
%token WHILE
%token IF
%token ELSE
%token FOR
%token PRINT
%token BOOLEAN 
%token TRUE
%token FALSE
%token IN
%token REPL_QUIT
%token EQUAL
%token PLUS
%token MINUS
%token MULTI
%token DIV
%token PERCENT
%token LPAREN
%token RPAREN
%token LBRACE
%token RBRACE
%token GREATER
%token LESSER
%token SEMICOLON
%token COMMA
%token EOF

%left PLUS MINUS
%left MULTI DIV
%left PERCENT
%left ELSE
%nonassoc UMINUS

%type <Syntax.statement> statement
%start statement

%%

statement:
| VAR EQUAL expression
    { Assign($1, $3) }
| FUNC VAR statement
    { AssignFunc($2, $3) }
| VAR LPAREN caller_args RPAREN
    { Call($1) }
| WHILE comparison statement
    { While($2, $3) }
| IF comparison statement ELSE statement
    { IfElse($2, $3, $5) }
| IF comparison statement
    { If($2, $3) }
| FOR VAR IN expression COMMA expression COMMA expression statement
    { For($2, $4, $6, $8, $9)}
| FOR VAR IN expression COMMA expression statement
    { For($2, $4, $6, EConst(VInt(1)), $7) }
| LBRACE statement_list RBRACE
    { Seq($2) }
| PRINT expression
    { Print($2) }
| REPL_QUIT
    { exit 0 }
| error
    { failwith
        (Printf.sprintf
            "parse error near characters %d-%d"
            (Parsing.symbol_start ())
            (Parsing.symbol_end ())) }


statement_list:
| statement SEMICOLON statement_list
    { $1 :: $3 }
|
    { [] }


caller_args:
| expression COMMA caller_args
    { $1 :: $3 }
|
    { [] }


expression:
| CONST
    { EConst($1) }
| VAR
    { EVar($1) }
| MINUS expression %prec UMINUS
    { EUmi($2) }
| expression PLUS expression
    { EAdd($1, $3) }
| expression MINUS expression
    { ESub($1, $3) }
| expression MULTI expression
    { EMul($1, $3) }
| expression DIV expression
    { EDiv($1, $3) }
| expression PERCENT expression
    { EMod($1, $3) }
| LPAREN expression RPAREN
    { $2 }


comparison:
| TRUE
    { CBool(true) }
| FALSE
    { CBool(false) }
| expression GREATER expression
    { Cgreater($1, $3) }
| expression GREATER EQUAL expression
    { Cgreatereq($1, $4) }
| expression LESSER EQUAL expression
    { Clessereq($1, $4) }
| expression LESSER expression
    { Clesser($1, $3) }
| expression EQUAL EQUAL expression
    { Ceq($1, $4) }
| expression NOT EQUAL expression
    { Cineq($1, $4) }
