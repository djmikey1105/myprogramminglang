type token =
  | VAR of (string)
  | CONST of (Syntax.vtype)
  | FCONST of (float)
  | STRING of (Syntax.vtype)
  | NOT
  | FUNC
  | WHILE
  | IF
  | ELSE
  | FOR
  | PRINT
  | BOOLEAN
  | TRUE
  | FALSE
  | IN
  | REPL_QUIT
  | EQUAL
  | PLUS
  | MINUS
  | MULTI
  | DIV
  | PERCENT
  | LPAREN
  | RPAREN
  | LBRACE
  | RBRACE
  | GREATER
  | LESSER
  | SEMICOLON
  | COMMA
  | EOF

open Parsing;;
let _ = parse_error;;
# 2 "parser.mly"
    open Syntax
# 39 "parser.ml"
let yytransl_const = [|
  261 (* NOT *);
  262 (* FUNC *);
  263 (* WHILE *);
  264 (* IF *);
  265 (* ELSE *);
  266 (* FOR *);
  267 (* PRINT *);
  268 (* BOOLEAN *);
  269 (* TRUE *);
  270 (* FALSE *);
  271 (* IN *);
  272 (* REPL_QUIT *);
  273 (* EQUAL *);
  274 (* PLUS *);
  275 (* MINUS *);
  276 (* MULTI *);
  277 (* DIV *);
  278 (* PERCENT *);
  279 (* LPAREN *);
  280 (* RPAREN *);
  281 (* LBRACE *);
  282 (* RBRACE *);
  283 (* GREATER *);
  284 (* LESSER *);
  285 (* SEMICOLON *);
  286 (* COMMA *);
    0 (* EOF *);
    0|]

let yytransl_block = [|
  257 (* VAR *);
  258 (* CONST *);
  259 (* FCONST *);
  260 (* STRING *);
    0|]

let yylhs = "\255\255\
\001\000\001\000\001\000\001\000\001\000\001\000\001\000\001\000\
\001\000\001\000\001\000\001\000\005\000\005\000\003\000\003\000\
\002\000\002\000\002\000\002\000\002\000\002\000\002\000\002\000\
\002\000\004\000\004\000\004\000\004\000\004\000\004\000\004\000\
\004\000\000\000"

let yylen = "\002\000\
\003\000\003\000\004\000\003\000\005\000\003\000\009\000\007\000\
\003\000\002\000\001\000\001\000\003\000\000\000\003\000\000\000\
\001\000\001\000\002\000\003\000\003\000\003\000\003\000\003\000\
\003\000\001\000\001\000\003\000\004\000\004\000\003\000\004\000\
\004\000\002\000"

let yydefred = "\000\000\
\000\000\000\000\012\000\000\000\000\000\000\000\000\000\000\000\
\000\000\011\000\000\000\034\000\000\000\000\000\000\000\018\000\
\017\000\026\000\027\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\002\000\
\019\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\004\000\000\000\000\000\000\000\009\000\
\000\000\003\000\025\000\000\000\000\000\000\000\000\000\000\000\
\000\000\024\000\000\000\000\000\000\000\000\000\000\000\000\000\
\013\000\015\000\000\000\000\000\000\000\000\000\005\000\000\000\
\000\000\000\000\008\000\000\000\007\000"

let yydgoto = "\002\000\
\027\000\022\000\031\000\023\000\028\000"

let yysindex = "\003\000\
\002\255\000\000\000\000\250\254\014\255\211\255\211\255\015\255\
\058\255\000\000\002\255\000\000\058\255\058\255\002\255\000\000\
\000\000\000\000\000\000\058\255\058\255\020\255\002\255\002\255\
\011\255\112\000\001\255\005\255\112\000\218\255\009\255\000\000\
\000\000\232\255\017\255\026\255\058\255\058\255\058\255\058\255\
\058\255\097\255\226\255\000\000\035\255\058\255\002\255\000\000\
\058\255\000\000\000\000\058\255\058\255\255\254\255\254\023\255\
\023\255\000\000\058\255\112\000\058\255\112\000\002\255\107\000\
\000\000\000\000\112\000\112\000\112\000\112\000\000\000\058\255\
\085\255\058\255\000\000\111\255\000\000"

let yyrindex = "\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\024\255\000\000\000\000\022\255\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\005\000\000\000\000\000\006\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\007\000\000\000\024\255\000\000\
\022\255\000\000\000\000\000\000\000\000\063\000\094\000\001\000\
\032\000\000\000\000\000\134\255\000\000\146\255\000\000\000\000\
\000\000\000\000\166\255\178\255\198\255\210\255\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000"

let yygindex = "\000\000\
\255\255\015\000\002\000\042\000\018\000"

let yytablesize = 393
let yytable = "\012\000\
\022\000\003\000\004\000\001\000\010\000\001\000\006\000\005\000\
\006\000\007\000\013\000\008\000\009\000\032\000\015\000\025\000\
\014\000\010\000\039\000\040\000\041\000\044\000\045\000\026\000\
\035\000\046\000\011\000\029\000\030\000\047\000\048\000\023\000\
\050\000\052\000\033\000\034\000\036\000\037\000\038\000\039\000\
\040\000\041\000\053\000\063\000\041\000\016\000\042\000\043\000\
\024\000\014\000\066\000\054\000\055\000\056\000\057\000\058\000\
\060\000\062\000\016\000\017\000\064\000\071\000\020\000\030\000\
\065\000\000\000\067\000\068\000\000\000\000\000\000\000\075\000\
\000\000\069\000\077\000\070\000\020\000\000\000\000\000\000\000\
\021\000\000\000\000\000\000\000\003\000\004\000\073\000\000\000\
\076\000\000\000\005\000\006\000\007\000\021\000\008\000\009\000\
\000\000\016\000\017\000\000\000\010\000\000\000\037\000\038\000\
\039\000\040\000\041\000\000\000\000\000\011\000\003\000\004\000\
\000\000\059\000\074\000\020\000\005\000\006\000\007\000\021\000\
\008\000\009\000\000\000\000\000\000\000\000\000\010\000\000\000\
\037\000\038\000\039\000\040\000\041\000\028\000\028\000\011\000\
\000\000\000\000\000\000\028\000\028\000\028\000\000\000\028\000\
\028\000\031\000\031\000\000\000\000\000\028\000\000\000\031\000\
\031\000\031\000\000\000\031\000\031\000\000\000\028\000\000\000\
\000\000\031\000\000\000\000\000\000\000\033\000\033\000\000\000\
\000\000\000\000\031\000\033\000\033\000\033\000\000\000\033\000\
\033\000\032\000\032\000\000\000\000\000\033\000\000\000\032\000\
\032\000\032\000\000\000\032\000\032\000\000\000\033\000\000\000\
\000\000\032\000\000\000\000\000\000\000\029\000\029\000\000\000\
\000\000\000\000\032\000\029\000\029\000\029\000\000\000\029\000\
\029\000\030\000\030\000\016\000\017\000\029\000\000\000\030\000\
\030\000\030\000\000\000\030\000\030\000\000\000\029\000\018\000\
\019\000\030\000\016\000\017\000\000\000\020\000\000\000\000\000\
\000\000\021\000\030\000\037\000\038\000\039\000\040\000\041\000\
\000\000\000\000\061\000\000\000\020\000\000\000\000\000\049\000\
\021\000\037\000\038\000\039\000\040\000\041\000\000\000\051\000\
\022\000\022\000\000\000\000\000\000\000\022\000\022\000\022\000\
\022\000\022\000\022\000\022\000\000\000\010\000\001\000\000\000\
\022\000\022\000\022\000\022\000\022\000\022\000\000\000\000\000\
\022\000\022\000\000\000\022\000\022\000\022\000\022\000\023\000\
\023\000\010\000\001\000\006\000\023\000\023\000\023\000\023\000\
\023\000\023\000\023\000\000\000\000\000\000\000\000\000\023\000\
\023\000\023\000\023\000\023\000\023\000\000\000\000\000\023\000\
\023\000\000\000\023\000\023\000\023\000\023\000\020\000\020\000\
\000\000\000\000\000\000\020\000\020\000\020\000\020\000\020\000\
\020\000\020\000\000\000\000\000\000\000\000\000\020\000\020\000\
\020\000\020\000\000\000\000\000\000\000\000\000\020\000\020\000\
\000\000\020\000\020\000\020\000\020\000\021\000\021\000\000\000\
\000\000\000\000\021\000\021\000\021\000\021\000\021\000\021\000\
\021\000\000\000\000\000\000\000\000\000\021\000\021\000\021\000\
\021\000\000\000\000\000\000\000\000\000\021\000\021\000\000\000\
\021\000\021\000\021\000\021\000\037\000\038\000\039\000\040\000\
\041\000\037\000\038\000\039\000\040\000\041\000\000\000\000\000\
\072\000"

let yycheck = "\001\000\
\000\000\000\001\001\001\001\000\000\000\000\000\000\000\006\001\
\007\001\008\001\017\001\010\001\011\001\015\000\001\001\001\001\
\023\001\016\001\020\001\021\001\022\001\023\000\024\000\009\000\
\005\001\015\001\025\001\013\000\014\000\029\001\026\001\000\000\
\024\001\017\001\020\000\021\000\017\001\018\001\019\001\020\001\
\021\001\022\001\017\001\009\001\022\001\024\001\027\001\028\001\
\007\000\026\001\049\000\037\000\038\000\039\000\040\000\041\000\
\042\000\043\000\001\001\002\001\046\000\063\000\000\000\049\000\
\047\000\255\255\052\000\053\000\255\255\255\255\255\255\073\000\
\255\255\059\000\076\000\061\000\019\001\255\255\255\255\255\255\
\023\001\255\255\255\255\255\255\000\001\001\001\072\000\255\255\
\074\000\255\255\006\001\007\001\008\001\000\000\010\001\011\001\
\255\255\001\001\002\001\255\255\016\001\255\255\018\001\019\001\
\020\001\021\001\022\001\255\255\255\255\025\001\000\001\001\001\
\255\255\017\001\030\001\019\001\006\001\007\001\008\001\023\001\
\010\001\011\001\255\255\255\255\255\255\255\255\016\001\255\255\
\018\001\019\001\020\001\021\001\022\001\000\001\001\001\025\001\
\255\255\255\255\255\255\006\001\007\001\008\001\255\255\010\001\
\011\001\000\001\001\001\255\255\255\255\016\001\255\255\006\001\
\007\001\008\001\255\255\010\001\011\001\255\255\025\001\255\255\
\255\255\016\001\255\255\255\255\255\255\000\001\001\001\255\255\
\255\255\255\255\025\001\006\001\007\001\008\001\255\255\010\001\
\011\001\000\001\001\001\255\255\255\255\016\001\255\255\006\001\
\007\001\008\001\255\255\010\001\011\001\255\255\025\001\255\255\
\255\255\016\001\255\255\255\255\255\255\000\001\001\001\255\255\
\255\255\255\255\025\001\006\001\007\001\008\001\255\255\010\001\
\011\001\000\001\001\001\001\001\002\001\016\001\255\255\006\001\
\007\001\008\001\255\255\010\001\011\001\255\255\025\001\013\001\
\014\001\016\001\001\001\002\001\255\255\019\001\255\255\255\255\
\255\255\023\001\025\001\018\001\019\001\020\001\021\001\022\001\
\255\255\255\255\017\001\255\255\019\001\255\255\255\255\030\001\
\023\001\018\001\019\001\020\001\021\001\022\001\255\255\024\001\
\000\001\001\001\255\255\255\255\255\255\005\001\006\001\007\001\
\008\001\009\001\010\001\011\001\255\255\009\001\009\001\255\255\
\016\001\017\001\018\001\019\001\020\001\021\001\255\255\255\255\
\024\001\025\001\255\255\027\001\028\001\029\001\030\001\000\001\
\001\001\029\001\029\001\029\001\005\001\006\001\007\001\008\001\
\009\001\010\001\011\001\255\255\255\255\255\255\255\255\016\001\
\017\001\018\001\019\001\020\001\021\001\255\255\255\255\024\001\
\025\001\255\255\027\001\028\001\029\001\030\001\000\001\001\001\
\255\255\255\255\255\255\005\001\006\001\007\001\008\001\009\001\
\010\001\011\001\255\255\255\255\255\255\255\255\016\001\017\001\
\018\001\019\001\255\255\255\255\255\255\255\255\024\001\025\001\
\255\255\027\001\028\001\029\001\030\001\000\001\001\001\255\255\
\255\255\255\255\005\001\006\001\007\001\008\001\009\001\010\001\
\011\001\255\255\255\255\255\255\255\255\016\001\017\001\018\001\
\019\001\255\255\255\255\255\255\255\255\024\001\025\001\255\255\
\027\001\028\001\029\001\030\001\018\001\019\001\020\001\021\001\
\022\001\018\001\019\001\020\001\021\001\022\001\255\255\255\255\
\030\001"

let yynames_const = "\
  NOT\000\
  FUNC\000\
  WHILE\000\
  IF\000\
  ELSE\000\
  FOR\000\
  PRINT\000\
  BOOLEAN\000\
  TRUE\000\
  FALSE\000\
  IN\000\
  REPL_QUIT\000\
  EQUAL\000\
  PLUS\000\
  MINUS\000\
  MULTI\000\
  DIV\000\
  PERCENT\000\
  LPAREN\000\
  RPAREN\000\
  LBRACE\000\
  RBRACE\000\
  GREATER\000\
  LESSER\000\
  SEMICOLON\000\
  COMMA\000\
  EOF\000\
  "

let yynames_block = "\
  VAR\000\
  CONST\000\
  FCONST\000\
  STRING\000\
  "

let yyact = [|
  (fun _ -> failwith "parser")
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : string) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expression) in
    Obj.repr(
# 50 "parser.mly"
    ( Assign(_1, _3) )
# 283 "parser.ml"
               : Syntax.statement))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 1 : string) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : Syntax.statement) in
    Obj.repr(
# 52 "parser.mly"
    ( AssignFunc(_2, _3) )
# 291 "parser.ml"
               : Syntax.statement))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 3 : string) in
    let _3 = (Parsing.peek_val __caml_parser_env 1 : 'caller_args) in
    Obj.repr(
# 54 "parser.mly"
    ( Call(_1) )
# 299 "parser.ml"
               : Syntax.statement))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 1 : 'comparison) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : Syntax.statement) in
    Obj.repr(
# 56 "parser.mly"
    ( While(_2, _3) )
# 307 "parser.ml"
               : Syntax.statement))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 3 : 'comparison) in
    let _3 = (Parsing.peek_val __caml_parser_env 2 : Syntax.statement) in
    let _5 = (Parsing.peek_val __caml_parser_env 0 : Syntax.statement) in
    Obj.repr(
# 58 "parser.mly"
    ( IfElse(_2, _3, _5) )
# 316 "parser.ml"
               : Syntax.statement))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 1 : 'comparison) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : Syntax.statement) in
    Obj.repr(
# 60 "parser.mly"
    ( If(_2, _3) )
# 324 "parser.ml"
               : Syntax.statement))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 7 : string) in
    let _4 = (Parsing.peek_val __caml_parser_env 5 : 'expression) in
    let _6 = (Parsing.peek_val __caml_parser_env 3 : 'expression) in
    let _8 = (Parsing.peek_val __caml_parser_env 1 : 'expression) in
    let _9 = (Parsing.peek_val __caml_parser_env 0 : Syntax.statement) in
    Obj.repr(
# 62 "parser.mly"
    ( For(_2, _4, _6, _8, _9))
# 335 "parser.ml"
               : Syntax.statement))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 5 : string) in
    let _4 = (Parsing.peek_val __caml_parser_env 3 : 'expression) in
    let _6 = (Parsing.peek_val __caml_parser_env 1 : 'expression) in
    let _7 = (Parsing.peek_val __caml_parser_env 0 : Syntax.statement) in
    Obj.repr(
# 64 "parser.mly"
    ( For(_2, _4, _6, EConst(VInt(1)), _7) )
# 345 "parser.ml"
               : Syntax.statement))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 1 : 'statement_list) in
    Obj.repr(
# 66 "parser.mly"
    ( Seq(_2) )
# 352 "parser.ml"
               : Syntax.statement))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'expression) in
    Obj.repr(
# 68 "parser.mly"
    ( Print(_2) )
# 359 "parser.ml"
               : Syntax.statement))
; (fun __caml_parser_env ->
    Obj.repr(
# 70 "parser.mly"
    ( exit 0 )
# 365 "parser.ml"
               : Syntax.statement))
; (fun __caml_parser_env ->
    Obj.repr(
# 72 "parser.mly"
    ( failwith
        (Printf.sprintf
            "parse error near characters %d-%d"
            (Parsing.symbol_start ())
            (Parsing.symbol_end ())) )
# 375 "parser.ml"
               : Syntax.statement))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : Syntax.statement) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'statement_list) in
    Obj.repr(
# 81 "parser.mly"
    ( _1 :: _3 )
# 383 "parser.ml"
               : 'statement_list))
; (fun __caml_parser_env ->
    Obj.repr(
# 83 "parser.mly"
    ( [] )
# 389 "parser.ml"
               : 'statement_list))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expression) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'caller_args) in
    Obj.repr(
# 88 "parser.mly"
    ( _1 :: _3 )
# 397 "parser.ml"
               : 'caller_args))
; (fun __caml_parser_env ->
    Obj.repr(
# 90 "parser.mly"
    ( [] )
# 403 "parser.ml"
               : 'caller_args))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : Syntax.vtype) in
    Obj.repr(
# 95 "parser.mly"
    ( EConst(_1) )
# 410 "parser.ml"
               : 'expression))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : string) in
    Obj.repr(
# 97 "parser.mly"
    ( EVar(_1) )
# 417 "parser.ml"
               : 'expression))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'expression) in
    Obj.repr(
# 99 "parser.mly"
    ( EUmi(_2) )
# 424 "parser.ml"
               : 'expression))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expression) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expression) in
    Obj.repr(
# 101 "parser.mly"
    ( EAdd(_1, _3) )
# 432 "parser.ml"
               : 'expression))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expression) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expression) in
    Obj.repr(
# 103 "parser.mly"
    ( ESub(_1, _3) )
# 440 "parser.ml"
               : 'expression))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expression) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expression) in
    Obj.repr(
# 105 "parser.mly"
    ( EMul(_1, _3) )
# 448 "parser.ml"
               : 'expression))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expression) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expression) in
    Obj.repr(
# 107 "parser.mly"
    ( EDiv(_1, _3) )
# 456 "parser.ml"
               : 'expression))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expression) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expression) in
    Obj.repr(
# 109 "parser.mly"
    ( EMod(_1, _3) )
# 464 "parser.ml"
               : 'expression))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 1 : 'expression) in
    Obj.repr(
# 111 "parser.mly"
    ( _2 )
# 471 "parser.ml"
               : 'expression))
; (fun __caml_parser_env ->
    Obj.repr(
# 116 "parser.mly"
    ( CBool(true) )
# 477 "parser.ml"
               : 'comparison))
; (fun __caml_parser_env ->
    Obj.repr(
# 118 "parser.mly"
    ( CBool(false) )
# 483 "parser.ml"
               : 'comparison))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expression) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expression) in
    Obj.repr(
# 120 "parser.mly"
    ( Cgreater(_1, _3) )
# 491 "parser.ml"
               : 'comparison))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 3 : 'expression) in
    let _4 = (Parsing.peek_val __caml_parser_env 0 : 'expression) in
    Obj.repr(
# 122 "parser.mly"
    ( Cgreatereq(_1, _4) )
# 499 "parser.ml"
               : 'comparison))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 3 : 'expression) in
    let _4 = (Parsing.peek_val __caml_parser_env 0 : 'expression) in
    Obj.repr(
# 124 "parser.mly"
    ( Clessereq(_1, _4) )
# 507 "parser.ml"
               : 'comparison))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expression) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expression) in
    Obj.repr(
# 126 "parser.mly"
    ( Clesser(_1, _3) )
# 515 "parser.ml"
               : 'comparison))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 3 : 'expression) in
    let _4 = (Parsing.peek_val __caml_parser_env 0 : 'expression) in
    Obj.repr(
# 128 "parser.mly"
    ( Ceq(_1, _4) )
# 523 "parser.ml"
               : 'comparison))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 3 : 'expression) in
    let _4 = (Parsing.peek_val __caml_parser_env 0 : 'expression) in
    Obj.repr(
# 130 "parser.mly"
    ( Cineq(_1, _4) )
# 531 "parser.ml"
               : 'comparison))
(* Entry statement *)
; (fun __caml_parser_env -> raise (Parsing.YYexit (Parsing.peek_val __caml_parser_env 0)))
|]
let yytables =
  { Parsing.actions=yyact;
    Parsing.transl_const=yytransl_const;
    Parsing.transl_block=yytransl_block;
    Parsing.lhs=yylhs;
    Parsing.len=yylen;
    Parsing.defred=yydefred;
    Parsing.dgoto=yydgoto;
    Parsing.sindex=yysindex;
    Parsing.rindex=yyrindex;
    Parsing.gindex=yygindex;
    Parsing.tablesize=yytablesize;
    Parsing.table=yytable;
    Parsing.check=yycheck;
    Parsing.error_function=parse_error;
    Parsing.names_const=yynames_const;
    Parsing.names_block=yynames_block }
let statement (lexfun : Lexing.lexbuf -> token) (lexbuf : Lexing.lexbuf) =
   (Parsing.yyparse yytables 1 lexfun lexbuf : Syntax.statement)
