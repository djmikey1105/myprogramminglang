type var = string

type vtype = 
    | VInt of int
    | VStr of string

type statement =
    | Assign of var * expression
    | AssignFunc of var * statement
    | Call of var
    | While of comparison * statement 
    | If of comparison * statement
    | IfElse of comparison * statement * statement
    | For of var * expression * expression * expression * statement
    | Seq of statement list
    | Print of expression

and expression =
    | EConst of vtype 
    | EVar of var
    | EUmi of expression
    | EAdd of expression * expression
    | ESub of expression * expression
    | EMul of expression * expression
    | EDiv of expression * expression
    | EMod of expression * expression

and comparison = 
    | CBool of bool
    | Cgreater of expression * expression
    | Cgreatereq of expression * expression
    | Clesser of expression * expression
    | Clessereq of expression * expression
    | Ceq of expression * expression
    | Cineq of expression * expression
