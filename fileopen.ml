open Interpret

let main =
    if Array.length Sys.argv = 2
    then
        let tmp = open_in Sys.argv.(1) in
        let s =
            Parser.statement Lexer.token
                (Lexing.from_channel tmp) in
            interpret s
    else
        Printf.printf "REPL mode\nuse :q to exit.\n";
        while true do
            let s =
                print_string ">> ";
                Parser.statement Lexer.token
                    (Lexing.from_string (read_line ())) in
                interpret s
        done
