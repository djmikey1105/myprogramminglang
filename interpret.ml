open Syntax

let table : (var, vtype) Hashtbl.t = Hashtbl.create 30
let func_table : (var, statement) Hashtbl.t = Hashtbl.create 20

let getIneqArith (a:int) = if a < 0 then (>) else (<)

let rec multString (s:string) (i:int) : string =
    if i = 1 then s else s ^ multString s (i-1)

let rec interpret (s:statement) : unit = match s with
    | Assign(x, e) ->
            let i = eval e in
            Hashtbl.replace table x i
    | AssignFunc(x, s2) ->
            Hashtbl.replace func_table x s2
    | Call(v) ->
            ( try
              let s2 = Hashtbl.find func_table v in
              interpret s2;
              with 
              | Not_found -> failwith "unknown function v"
            )
    | While(co, s2) ->
            if compare co then 
                (interpret s2;
                interpret s)
    | If(co, s2) ->
            if compare co then interpret s2
    | IfElse(co, s2, s3) ->
            if compare co then interpret s2 else interpret s3
    | For(v, e1, e2, e3, s2) ->
            let evConst a =
                ( match a with
                  | VInt vi -> vi
                  | VStr vs -> failwith "unsupported"
                )
            in
            let i1 = evConst (eval e1) in
            let i2 = evConst (eval e2) in
            let i_inc = evConst (eval e3) in
            let ineq_arith = getIneqArith i_inc in
            (try
                ( let hashv = evConst (Hashtbl.find table v) in
                  if ineq_arith hashv i2 then
                    (interpret s2;
                     Hashtbl.replace table v (VInt(hashv + i_inc));
                     interpret s);
                )
            with
            | Not_found ->
                Hashtbl.replace table v (VInt(i1));
                interpret s;
                )
    | Seq(ss) ->
            List.iter interpret ss
    | Print(e) ->
            let ev = eval e in
            match ev with
                | VInt(i) ->
                    Printf.printf "%d\n" i
                | VStr(i) ->
                    Printf.printf "%s\n" i
and eval (e:expression) : vtype = match e with
    | EConst i -> i
    | EUmi u ->
        let uu = eval u in
        (
            match uu with
            | VInt i -> VInt(-i)
            | VStr i ->
                    failwith("bad operand type '-'")
        )
    | EAdd(x, y) ->
        let xx = eval x in
        let yy = eval y in
        (
        match (xx, yy) with
            | (VInt i, VInt j) -> VInt(i + j)
            | (VStr i, VStr j) -> VStr(i ^ j)
            | (_, _) -> failwith ("unsupported")
        )
    | ESub(x, y) -> 
        let xx = eval x in
        let yy = eval y in
        (
        match (xx, yy) with
            | (VInt i, VInt j) -> VInt(i - j)
            | (_, _) -> failwith ("unsupported")
        )
    | EMul(x, y) -> 
        let xx = eval x in
        let yy = eval y in
        (
        match (xx, yy) with
            | (VInt i, VInt j) -> VInt(i * j)
            | (VStr i, VInt j) -> VStr(multString i j)
            | (_, _) -> failwith ("unsupported")
        )
    | EDiv(x, y) ->
        let xx = eval x in
        let yy = eval y in
        (
        match (xx, yy) with
            | (VInt i, VInt j) -> VInt(i / j)
            | (_, _) -> failwith ("unsupported")
        )
    | EMod(x, y) ->
        let xx = eval x in
        let yy = eval y in
        ( match (xx, yy) with
          | (VInt i, VInt j) -> VInt(i mod j)
          | (_, _) -> failwith "unsupported"
        )
    | EVar v ->
            (try (Hashtbl.find table v)
            with
            Not_found ->
                failwith ("Unknown variable " ^ v))
(* and eval (v:vtype) : vtype = match v with *)
(*     | VInt i -> i *)
(*     | VStr s -> s *)
and compare (c:comparison) : bool = match c with
    | CBool(b) -> b
    | Cgreater(x, y) -> eval x > eval y
    | Cgreatereq(x, y) -> eval x >= eval y
    | Clessereq(x, y) -> eval x <= eval y
    | Clesser(x, y) -> eval x < eval y
    | Ceq(x, y) -> eval x == eval y
    | Cineq(x, y) -> eval x != eval y
