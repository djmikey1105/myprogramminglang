{
    open Parser
    open Syntax
}

let space = [' ' '\t' '\n' '\r']
let digit = ['0'-'9']
let alpha = ['A'-'Z' 'a'-'z' '_']
let symbol = ['!' '#'-'/' ':'-'@' '['-'`' '{'-'^']
let dquote = '"'

rule token = parse
| "not"
    { NOT }
| "func"
    { FUNC }
| "while"
    { WHILE }
| "if"
    { IF }
| "else"
    { ELSE }
| "for"
    { FOR }
| "print"
    { PRINT }
| "True"
    { TRUE }
| "False"
    { FALSE }
| "in"
    { IN }
| ":q"
    { REPL_QUIT }
| dquote (alpha|digit|space|symbol)* dquote
    { let s = Lexing.lexeme lexbuf in
      CONST(VStr(Bytes.sub s 1 ((Bytes.length s) - 2))) }
| alpha (digit|alpha)*
    { VAR(Lexing.lexeme lexbuf) }
| '-'? digit+
    { CONST(VInt(int_of_string (Lexing.lexeme lexbuf) ) ) }
| '-'? digit+ '.' digit+
    { FCONST(float_of_string (Lexing.lexeme lexbuf)) }
| space+
    { token lexbuf }
| '!'
    { NOT }
| '='
    { EQUAL }
| '+'
    { PLUS }
| '-'
    { MINUS }
| '*'
    { MULTI }
| '/'
    { DIV }
| '%'
    { PERCENT }
| '('
    { LPAREN }
| ')'
    { RPAREN }
| '{'
    { LBRACE }
| '}'
    { RBRACE }
| '>'
    { GREATER }
| '<'
    { LESSER }
| ';'
    { SEMICOLON }
| ','
    { COMMA }
| eof
    { EOF }
| _
    { failwith
        (Printf.sprintf
            "unknown token %s near characters %d-%d"
            (Lexing.lexeme lexbuf)
            (Lexing.lexeme_start lexbuf)
            (Lexing.lexeme_end lexbuf)) }
